import { MyTeam, Stats, Search, Tags, CharacterTable } from 'components';

const Home = () => {
  return (
    <main className="container p-4 mx-auto md:p-6">
      <MyTeam />
      <Stats />
      <Search />
      <Tags />
      <CharacterTable />
    </main>
  );
};

export default Home;
