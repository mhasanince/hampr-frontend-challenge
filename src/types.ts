export enum ABILITY {
  POWER = 'Power',
  MOBILITY = 'Mobility',
  TECHNIQUE = 'Technique',
  SURVAVIBILITY = 'Survivability',
  ENERGY = 'Energy',
}

export interface CharacterAbility {
  abilityName: ABILITY;
  abilityScore: number;
}

export interface CharacterTag {
  slot: number;
  tag_name: string;
}

export interface Character {
  id: number;
  name: string;
  quote: string;
  image: string;
  thumbnail: string;
  universe: string;
  abilities: CharacterAbility[];
  tags: CharacterTag[] | undefined;
}

export type ActionMap<T extends { [index: string]: any }> = {
  [Key in ACTION_TYPE]: Key extends keyof T
    ? {
        type: Key;
        payload: T[Key];
      }
    : {
        type: Key;
      };
};

export enum ACTION_TYPE {
  TOGGLE_CHARACTER = 'TOGGLE_CHARACTER',
  REMOVE_CHARACTER = 'REMOVE_CHARACTER',
  SET_SEARCH_FILTER = 'SET_SEARCH_FILTER',
  TOGGLE_TAG_FILTER = 'TOGGLE_TAG_FILTER',
  CLEAR_TAGS = 'CLEAR_TAGS',
  SET_TAB = 'SET_TAB',
}

export interface CharacterContextState {
  characters: Character[];
  filteredCharacters: Character[];
  selectedCharacters: Character[];
  tab: TAB;
  filters: {
    search: string;
    tags: string[];
  };
}

export interface Payload {
  [ACTION_TYPE.TOGGLE_CHARACTER]: Character;
  [ACTION_TYPE.REMOVE_CHARACTER]: number;
  [ACTION_TYPE.SET_SEARCH_FILTER]: string;
  [ACTION_TYPE.TOGGLE_TAG_FILTER]: string;
  [ACTION_TYPE.SET_TAB]: TAB;
}

export type Action = ActionMap<Payload>[keyof ActionMap<Payload>];

export enum TAB {
  ALL_CHARACTERS = 'ALL_CHARACTERS',
  MY_TEAM = 'MY_TEAM',
}
