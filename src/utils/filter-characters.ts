import { Character, CharacterContextState, TAB } from 'types';

export const filterCharacters = (state: CharacterContextState) => {
  const filterBySearch = (character: Character) => {
    const { name, tags } = character;
    const search = state.filters.search.toLowerCase();

    return (
      name.toLowerCase().includes(search) ||
      tags?.some((tag) => tag.tag_name.toLowerCase().includes(search))
    );
  };

  const filterByTags = (character: Character) =>
    state.filters.tags.every((tag) =>
      character.tags?.some(
        (characterTag) => characterTag.tag_name === tag.toLowerCase()
      )
    );

  const characters =
    state.tab === TAB.ALL_CHARACTERS
      ? state.characters
      : state.selectedCharacters;

  return characters.filter(filterBySearch).filter(filterByTags);
};
