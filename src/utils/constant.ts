import jsonData from 'data/characters.json';
import type { Character } from 'types';
const data = jsonData as Character[];

export const TAGS = Array.from(
  new Set(data.map(({ tags }) => tags?.map((tag) => tag.tag_name)).flat())
).filter((tag) => tag);
