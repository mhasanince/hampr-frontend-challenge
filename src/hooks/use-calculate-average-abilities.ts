import { useCharacterContext } from 'context/character-context';
import { ABILITY } from 'types';

export const useCalculateAverageAbilities = () => {
  const [state] = useCharacterContext();

  return Object.values(ABILITY)
    .map((ability) => {
      const abilityScore = state.selectedCharacters.reduce(
        (acc, character) =>
          acc +
          (character.abilities.find(
            (characterAbility) => characterAbility.abilityName === ability
          )?.abilityScore ?? 0),
        0
      );

      return {
        name: ability,
        average: abilityScore / state.selectedCharacters.length || 0,
      };
    })
    .map((ability) => ({
      ...ability,
      average:
        ability.average % 1 === 0
          ? ability.average
          : ability.average.toFixed(2),
    }));
};
