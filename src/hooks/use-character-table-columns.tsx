import { createColumnHelper } from '@tanstack/react-table';
import clsx from 'clsx';
import { Avatar, Tag } from 'components';
import { useCharacterContext } from 'context/character-context';
import { ABILITY, Character } from 'types';

const columnHelper = createColumnHelper<Character>();

export const useCharacterTableColumns = () => {
  const [state] = useCharacterContext();

  const columns = [
    columnHelper.accessor((row) => row, {
      id: 'name',
      header: 'Name',
      cell: (info) => {
        const row = info.getValue();

        return (
          <div className="flex items-center gap-4">
            <input
              type="checkbox"
              className="w-6 h-6 border-2 rounded shrink-0 border-primary-500"
              checked={state.selectedCharacters.some(
                (character) => character.id === row.id
              )}
              readOnly
            />
            <Avatar src={row.image} alt={row.name} />
            <span>{row.name}</span>
          </div>
        );
      },
    }),
    columnHelper.accessor('tags', {
      header: 'Tags',
      cell: (info) => {
        const tags = info.getValue()?.sort((a, b) => a.slot - b.slot);

        return (
          <ul className="flex items-center gap-3">
            {tags?.map((tag, index) => (
              <li key={index}>
                <Tag name={tag.tag_name} />
              </li>
            ))}
          </ul>
        );
      },
    }),
    ...Object.values(ABILITY).map((ability) =>
      columnHelper.accessor('abilities', {
        id: ability.toLowerCase(),
        header: ability,
        cell: (info) => {
          const abilities = info.getValue();
          const foundAbility = abilities.find((a) => a.abilityName === ability);
          const score = foundAbility?.abilityScore;

          return (
            <p
              className={clsx(
                'text-center font-medium',
                score === 10 && 'text-red-500'
              )}
            >
              {score}
            </p>
          );
        },
      })
    ),
  ];

  return columns;
};
