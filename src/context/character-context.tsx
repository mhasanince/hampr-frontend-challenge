import { createContext, useContext, useReducer, Dispatch } from 'react';
import { toast } from 'react-toastify';
import jsonData from 'data/characters.json';
import {
  Action,
  ACTION_TYPE,
  Character,
  CharacterContextState,
  TAB,
} from 'types';
import { filterCharacters } from 'utils/filter-characters';
const data = jsonData as Character[];

const initialState: CharacterContextState = {
  characters: data,
  filteredCharacters: data,
  selectedCharacters: [],
  tab: TAB.ALL_CHARACTERS,
  filters: {
    search: '',
    tags: [],
  },
};

const CharacterContext = createContext<{
  state: CharacterContextState;
  dispatch: Dispatch<Action>;
}>({ state: initialState, dispatch: () => {} });

function characterReducer(
  state: CharacterContextState,
  action: Action
): CharacterContextState {
  switch (action.type) {
    case ACTION_TYPE.TOGGLE_CHARACTER: {
      const id = action.payload.id;
      const isSelected = state.selectedCharacters.some(
        (character) => character.id === id
      );

      if (!isSelected && state.selectedCharacters.length === 6) {
        toast.error('You can only select up to 6 characters');
        return { ...state };
      }

      const selectedCharacters = isSelected
        ? state.selectedCharacters.filter((character) => character.id !== id)
        : [...state.selectedCharacters, action.payload];

      return {
        ...state,
        filteredCharacters: filterCharacters({ ...state, selectedCharacters }),
        selectedCharacters,
      };
    }
    case ACTION_TYPE.SET_SEARCH_FILTER: {
      const filters = {
        ...state.filters,
        search: action.payload,
      };

      return {
        ...state,
        filteredCharacters: filterCharacters({ ...state, filters }),
        filters,
      };
    }
    case ACTION_TYPE.TOGGLE_TAG_FILTER: {
      const tag = action.payload;
      const tagIncludes = state.filters.tags.includes(tag);
      const tags = tagIncludes
        ? state.filters.tags.filter((t) => t !== tag)
        : [...state.filters.tags, tag];

      const filters = {
        ...state.filters,
        tags,
      };

      return {
        ...state,
        filteredCharacters: filterCharacters({ ...state, filters }),
        filters,
      };
    }
    case ACTION_TYPE.CLEAR_TAGS: {
      const filters = {
        ...state.filters,
        tags: [],
      };

      return {
        ...state,
        filteredCharacters: filterCharacters({ ...state, filters }),
        filters,
      };
    }
    case ACTION_TYPE.SET_TAB: {
      const tab = action.payload;

      return {
        ...state,
        filteredCharacters: filterCharacters({
          ...state,
          tab,
        }),
        tab,
      };
    }
    default: {
      throw new Error(`Unhandled action type`);
    }
  }
}

function CharacterProvider(props: { children: React.ReactNode }) {
  const { children } = props;
  const [state, dispatch] = useReducer(characterReducer, initialState);
  const value = { state, dispatch };
  return (
    <CharacterContext.Provider value={value}>
      {children}
    </CharacterContext.Provider>
  );
}

function useCharacterContext() {
  const context = useContext(CharacterContext);
  if (context === undefined) {
    throw new Error('useCharacters must be used within a CharacterProvider');
  }
  return [context.state, context.dispatch] as const;
}

export { CharacterProvider, useCharacterContext };
