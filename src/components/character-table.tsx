import {
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import clsx from 'clsx';
import { useCharacterContext } from 'context/character-context';
import { useCharacterTableColumns } from 'hooks';
import { ACTION_TYPE, TAB } from 'types';

export interface TabButtonProps
  extends React.HTMLAttributes<HTMLButtonElement> {
  children: React.ReactNode;
  isSelected: boolean;
}

const TabButton = (props: TabButtonProps) => {
  const { children, isSelected, ...rest } = props;
  return (
    <button
      type="button"
      className={clsx(
        'p-2 border-b-2 font-medium hover:bg-primary-200 rounded-t',
        isSelected ? 'border-primary-500' : 'border-primary-200'
      )}
      {...rest}
    >
      {children}
    </button>
  );
};

export const CharacterTable = () => {
  const [state, dispatch] = useCharacterContext();
  const columns = useCharacterTableColumns();
  const table = useReactTable({
    data: state.filteredCharacters,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <section>
      <div className="grid grid-cols-2 mb-2">
        <TabButton
          isSelected={state.tab === TAB.ALL_CHARACTERS}
          onClick={() =>
            dispatch({ type: ACTION_TYPE.SET_TAB, payload: TAB.ALL_CHARACTERS })
          }
        >
          All Characters
        </TabButton>
        <TabButton
          isSelected={state.tab === TAB.MY_TEAM}
          onClick={() =>
            dispatch({ type: ACTION_TYPE.SET_TAB, payload: TAB.MY_TEAM })
          }
        >
          My Team
        </TabButton>
      </div>
      <div className="max-h-[500px] sm:max-h-[675px] overflow-auto">
        <table className="w-full">
          <thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => (
                  <th
                    key={header.id}
                    className="sticky top-0 px-3 pb-2 text-lg font-semibold bg-primary-50 first:px-0 last:px-0"
                  >
                    <div className="min-w-max">
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody>
            {state.filteredCharacters.length === 0 ? (
              <tr>
                <td
                  colSpan={7}
                  className="p-4 text-lg text-center text-secondary-400"
                >
                  No characters found.
                </td>
              </tr>
            ) : (
              table.getRowModel().rows.map((row) => {
                const rowCharacter = row.original;
                const isSelected = state.selectedCharacters.some(
                  (character) => character.id === rowCharacter.id
                );

                return (
                  <tr
                    key={row.id}
                    className={clsx(
                      'hover:bg-primary-200 hover:cursor-pointer',
                      isSelected ? 'bg-primary-200' : 'bg-white'
                    )}
                    onClick={() =>
                      dispatch({
                        type: ACTION_TYPE.TOGGLE_CHARACTER,
                        payload: rowCharacter,
                      })
                    }
                  >
                    {row.getVisibleCells().map((cell) => (
                      <td
                        key={cell.id}
                        className="px-3 py-4 border-b border-secondary-100"
                      >
                        <div className="min-w-max">
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )}
                        </div>
                      </td>
                    ))}
                  </tr>
                );
              })
            )}
          </tbody>
        </table>
      </div>
    </section>
  );
};
