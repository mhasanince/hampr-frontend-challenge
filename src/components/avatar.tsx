import { XMarkIcon } from '@heroicons/react/24/outline';
import clsx from 'clsx';

export interface AvatarProps
  extends Omit<React.HtmlHTMLAttributes<HTMLButtonElement>, 'onClick'> {
  src: string;
  alt: string;
  size?: number;
  showRemove?: boolean;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export const Avatar = (props: AvatarProps) => {
  const { src, alt, size = 40, showRemove = false, onClick, ...rest } = props;

  return (
    <div className="relative">
      <button
        type="button"
        className={clsx(
          'overflow-hidden border rounded-full shrink-0 border-primary-500 transition-all',
          showRemove && [
            'relative sm:hover:border-red-500',
            'after:hidden sm:after:flex sm:after:items-center sm:after:justify-center',
            'after:content-["Remove"] after:text-white after:bg-red-500 after:bg-opacity-75',
            'after:invisible hover:after:visible after:opacity-0 hover:after:opacity-100 after:absolute after:inset-0',
            'after:w-full after:h-full after:z-50 after:transition-all',
          ]
        )}
        onClick={onClick}
        {...rest}
      >
        <img src={src} alt={alt} width={size} height={size} />
      </button>
      {showRemove && (
        <button
          className="absolute right-0 z-50 w-6 h-6 p-1 text-white bg-red-500 rounded-full sm:hidden"
          onClick={onClick}
        >
          <XMarkIcon className="stroke-[3]" />
        </button>
      )}
    </div>
  );
};
