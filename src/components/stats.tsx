import clsx from 'clsx';
import { useCalculateAverageAbilities } from 'hooks';
import { ABILITY } from 'types';

export const Stats = () => {
  const calculatedAbilities = useCalculateAverageAbilities();

  return (
    <section className="max-w-xl mx-auto mb-8">
      <div className="grid grid-cols-2 sm:grid-cols-5">
        {calculatedAbilities.map((ability) => (
          <div
            key={ability.name}
            className={clsx(
              'flex flex-col items-center justify-between p-2',
              ability.name === ABILITY.TECHNIQUE && [
                'relative col-span-2 sm:col-span-1',
                'before:absolute before:w-2/3 before:h-[1px] before:top-0 before:bg-divider-x',
                'sm:before:w-[1px] sm:before:h-full sm:before:left-0 sm:before:bg-divider-y',
                'after:absolute after:w-2/3 after:h-[1px] after:bottom-0 after:bg-divider-x',
                'sm:after:w-[1px] sm:after:h-full sm:after:right-0 sm:after:bg-divider-y',
              ]
            )}
          >
            <span>{ability.name}</span>
            <span className="text-2xl font-bold">{ability.average || '-'}</span>
          </div>
        ))}
      </div>
      <span className="text-xs text-secondary-500">
        * Totals as average for squad
      </span>
    </section>
  );
};
