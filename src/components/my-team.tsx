import { useCharacterContext } from 'context/character-context';
import { ACTION_TYPE } from 'types';
import { Avatar } from './avatar';

export const MyTeam = () => {
  const [state, dispatch] = useCharacterContext();

  return (
    <section className="flex flex-col items-center gap-4 mb-4">
      <h3 className="text-2xl font-bold text-center">
        {state.selectedCharacters.length
          ? 'Your Champions!'
          : 'Select Your Squad to Defend Earthrealm!'}
      </h3>
      <div className="flex flex-wrap justify-center gap-2 transition-all">
        {state.selectedCharacters.map((character) => (
          <Avatar
            key={character.id}
            src={character.image}
            alt={character.name}
            size={80}
            showRemove
            onClick={() =>
              dispatch({
                type: ACTION_TYPE.TOGGLE_CHARACTER,
                payload: character,
              })
            }
          />
        ))}
      </div>
    </section>
  );
};
