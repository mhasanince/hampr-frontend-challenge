import clsx from 'clsx';
import { useCharacterContext } from 'context/character-context';
import { ACTION_TYPE } from 'types';
import { CheckIcon } from '@heroicons/react/24/outline';

export interface TagProps
  extends Omit<React.HTMLAttributes<HTMLButtonElement>, 'onClick'> {
  name: string;
  className?: string;
  isSelected?: boolean;
}

export const Tag = (props: TagProps) => {
  const { name, className = '', isSelected = false, ...rest } = props;
  const [_, dispatch] = useCharacterContext();

  return (
    <button
      type="button"
      className={clsx(
        'flex items-center justify-center border rounded-full  border-primary-500 p-2.5 transition-all',
        isSelected
          ? 'bg-primary-500 text-white hover:bg-primary-500'
          : 'bg-white text-primary-500 hover:bg-primary-400 hover:text-white',
        className
      )}
      onClick={(e) => {
        e.stopPropagation();

        dispatch({
          type: ACTION_TYPE.TOGGLE_TAG_FILTER,
          payload: name,
        });
      }}
      {...rest}
    >
      <CheckIcon
        className={clsx(
          'text-white transition-all',
          isSelected ? 'w-4 h-4 mr-1 stroke-[3]' : 'w-0 h-0'
        )}
      />
      {`${name[0].toUpperCase()}${name.slice(1)}`}
    </button>
  );
};
