import clsx from 'clsx';
import { useCharacterContext } from 'context/character-context';
import { MagnifyingGlassIcon, XMarkIcon } from '@heroicons/react/24/outline';
import { ACTION_TYPE } from 'types';

export const Search = () => {
  const [state, dispatch] = useCharacterContext();

  return (
    <section
      className={clsx(
        'relative max-w-xl mx-auto mb-8',
        'before:absolute before:w-0 lg:before:w-[calc(100%+28rem)] before:inset-1/2 before:-translate-x-1/2 before:-translate-y-1/2 before:h-[1px] before:bg-divider-x before:-z-10'
      )}
    >
      <MagnifyingGlassIcon className="absolute left-2.5 w-5 h-5 -translate-y-1/2 inset-y-1/2 text-secondary-400" />
      <input
        type="text"
        className="w-full border border-secondary-400 text-secondary-400 rounded-[4px] py-3 px-9 placeholder:text-secondary-400 bg-white"
        placeholder="Search for Character or Tag"
        value={state.filters.search}
        onChange={(e) => {
          dispatch({
            type: ACTION_TYPE.SET_SEARCH_FILTER,
            payload: e.target.value,
          });
        }}
      />
      {state.filters.search.length > 0 && (
        <button
          type="button"
          className="absolute right-2.5 -translate-y-1/2 inset-y-1/2 w-6 h-6 p-1 hover:bg-secondary-100 rounded"
          onClick={() => {
            dispatch({
              type: ACTION_TYPE.SET_SEARCH_FILTER,
              payload: '',
            });
          }}
        >
          <XMarkIcon className="text-secondary-400 stroke-[3]" />
        </button>
      )}
    </section>
  );
};
