import MortalKombatLogo from 'img/Mortal-Kombat-Logo.png';

export const Navbar = () => {
  return (
    <nav className="w-full h-[76px] mb-[46px] bg-black flex items-start justify-center sticky top-0 z-50">
      <img
        src={MortalKombatLogo}
        alt="Mortal Kombat Logo"
        className="w-40 mt-8"
      />
    </nav>
  );
};
