import { useCharacterContext } from 'context/character-context';
import { ACTION_TYPE } from 'types';
import { TAGS } from 'utils/constant';
import { Tag } from './tag';

export const Tags = () => {
  const [state, dispatch] = useCharacterContext();

  return (
    <section className="mb-8">
      <ul className="flex flex-wrap items-center gap-2.5">
        {TAGS.map((tag) => {
          const isSelected = state.filters.tags.includes(tag!);

          return (
            <li key={tag}>
              <Tag name={tag!} isSelected={isSelected} />
            </li>
          );
        })}
        <li>
          <button
            type="button"
            className="leading-4 border-b-2 text-secondary-400 border-b-transparent hover:border-b-secondary-400"
            onClick={() =>
              dispatch({
                type: ACTION_TYPE.CLEAR_TAGS,
              })
            }
          >
            Clear All Tags
          </button>
        </li>
      </ul>
    </section>
  );
};
