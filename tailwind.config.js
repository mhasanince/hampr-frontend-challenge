/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          500: '#217AFF',
          400: '#5E9CFF',
          200: '#EDF5FF',
          50: '#F5FDFF',
        },
        secondary: {
          600: '#555555',
          500: '#666666',
          400: '#777777',
          300: '#999999',
          100: '#EEEEEE',
          50: '#F8F8F8',
        },
      },
      backgroundImage: {
        'divider-x':
          'linear-gradient(90deg, rgba(0, 0, 0, 0) 0%, #000000 50%, rgba(0, 0, 0, 0) 100%)',
        'divider-y':
          'linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 50%, rgba(0, 0, 0, 0) 100%)',
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
